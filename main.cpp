/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: testing main of ImageFusion
LANG: C++
*/

#include<iostream>
#include<vector>
#include<fstream>
#include "opencv2/opencv.hpp"
#include "ImageFusion.h"

using namespace std;
using namespace cv;



struct selecter
{
  int status;
  vector<Point> cont;
  Mat oImage,showImage,backImage,showImage2;
  void reset(){status=0;};
  Point2i offset,fst;
};

inline double dist(const Point &a,const Point &b)
{
  return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}

selecter sel;

void onMouse2(int event,int x,int y,int flags,void* param)  
{  
  if((sel.status==4) && event == CV_EVENT_LBUTTONUP)
  {
    sel.offset=Point2i(x,y)-sel.fst;
    cout<<"offset is ( "<<sel.offset.x<<','<<sel.offset.y<<" )"<<endl;
    sel.status=5;
  }
}

void onMouse(int event,int x,int y,int flags,void* param)  
{  
    Point p1,p2;  
    if(event==CV_EVENT_LBUTTONDOWN && (sel.status==0))  
    {
      sel.offset=Point2i(0,0);
      sel.cont.clear();
      sel.cont.push_back(Point(x,y));
      sel.oImage.copyTo(sel.showImage);
      sel.status++;
      return;
    }  
    if( (sel.status==1)  &&event == CV_EVENT_MOUSEMOVE)  
    {
      if (dist(sel.cont[sel.cont.size()-1],Point(x,y))>=2 && x>=0 && x <sel.oImage.cols && y>=0 && y<sel.oImage.rows)
      {
        sel.cont.push_back(Point(x,y));
        line(sel.showImage,sel.cont[sel.cont.size()-2],sel.cont[sel.cont.size()-1],Scalar(0,255,0));
        imshow("fore",sel.showImage);
      }
      return;
    }  
    if((sel.status==1) && event == CV_EVENT_LBUTTONUP)  
    {
      sel.status=3;
      if (x>=0 && x <sel.oImage.cols && y>=0 && y<sel.oImage.rows)
      {
        sel.cont.push_back(Point(x,y));
        sel.oImage.copyTo(sel.showImage);
        sel.backImage.copyTo(sel.showImage2);
        vector< vector<Point> > mycont(1,sel.cont);
        drawContours(sel.showImage,mycont,-1,Scalar(0,255,0),2);
        waitKey(200);
        Mat myMask=Mat::zeros(sel.oImage.rows,sel.oImage.cols,CV_8UC1);
        drawContours(myMask,mycont,-1,Scalar(255),CV_FILLED);
        drawContours(sel.showImage2,mycont,-1,Scalar(0,255,0),2);
        int mx=0,my=0,tx=sel.oImage.cols,ty=sel.oImage.rows;
        for (int i=0;i<sel.cont.size();i++)
        {
          mx=max(mx,sel.cont[i].x);
          my=max(my,sel.cont[i].y);
          tx=min(tx,sel.cont[i].x);
          ty=min(ty,sel.cont[i].y);
        }
        sel.fst.x=(mx+tx)/2;
        sel.fst.y=(my+ty)/2;
        circle(sel.showImage2,sel.fst,2,Scalar(0,255,0),CV_FILLED);
        circle(sel.showImage,sel.fst,2,Scalar(0,255,0),CV_FILLED);
        imshow("fore",sel.showImage);
        imshow("new",sel.showImage2);
        Mat res;
        cout<<"make offset please"<<endl;
        sel.status=4;
        //Point tmp;
        //cin>>tmp.x>>tmp.y;
        while (sel.status==4)
          waitKey(100);
        if (sel.cont.size()<5)
        {
          cout<<"no mask or mask too small, use no mask mode"<<endl;
          Mat al1=Mat::zeros(sel.backImage.rows,sel.backImage.cols,CV_8UC1);
          Mat al2=Mat::zeros(sel.oImage.rows,sel.oImage.cols,CV_8UC1);
          Mat bimg(sel.backImage.rows,sel.backImage.cols,CV_8UC4);
          Mat fimg(sel.oImage.rows,sel.oImage.cols,CV_8UC4);
          Mat input[] = { sel.backImage, al1 };
          int from_to[] = { 0,2, 1,1, 2,0, 3,3 };
          mixChannels( input, 2, &bimg, 1, from_to, 4 );
          Mat input2[]={sel.oImage,al2};
          mixChannels( input2, 2, &fimg, 1, from_to, 4 );
          Mat tmp1;
          ImageFuser::imageFusion(bimg, fimg, tmp1,sel.offset, Mat());
          int from_to2[] = { 0,2, 1,1, 2,0, 3,3 };
          res.create(bimg.rows,bimg.cols,CV_8UC3);
          mixChannels( &tmp1,1,&res,1,from_to2,3);
        }
        else
          ImageFuser::imageFusion(sel.backImage, sel.oImage, res,sel.offset, myMask);
        imshow("new",res);
        waitKey(200);
        imwrite("new.jpg",res);
      }
      else
      {
        cout<<"not valid"<<endl;
      }
      sel.status=0;
    }     
}  
int main(int argc, char *argv[])
{
  if (argc<2)
  {
    cout<<"please give background and foreground image"<<endl;
    return-1;
  }
  if (argc==2)
  {
    string foreP="fore/",bgP="bg/",listP="list.txt",resP="result/";
    vector<Mat> foreS,bgS;
    ifstream foreIn((foreP+listP).c_str()),bgIn((bgP+listP).c_str());
    vector<string> foreN,bgN;
    string tmp;
    while (foreIn>>tmp)
    {
      foreN.push_back(tmp);
      Mat mtmp=imread(foreP+tmp,-1);
      foreS.push_back(mtmp);
    }
    while (bgIn>>tmp)
    {
      bgN.push_back(tmp);
      Mat mtmp=imread(bgP+tmp,-1);
      bgS.push_back(mtmp);
    }
    for (int i=0;i<foreS.size();i++)
      for (int j=0;j<bgS.size();j++)
      {
        Mat rtmp,res(bgS[j].rows,bgS[j].cols,CV_8UC3);
        ImageFuser::imageFusion(bgS[j], foreS[i], rtmp,Point(0,0), Mat());
        // int from_to[] = { 0,2, 1,1, 2,0 };
        // mixChannels( &rtmp, 1, &res, 1, from_to, 3 );
        imwrite(resP+foreN[i]+"__"+bgN[j]+"_.png",rtmp);
      }
  }
  else
  {
    Mat myimage=imread(argv[1]),tgtimage=imread(argv[2]);
    namedWindow("fore");
    myimage.copyTo(sel.oImage);
    tgtimage.copyTo(sel.backImage);
    namedWindow("new");
    imshow("fore",sel.oImage);
    imshow("new",tgtimage);
    sel.reset();
    setMouseCallback("fore",onMouse,0);
    setMouseCallback("new",onMouse2,0);
    waitKey(0);
  }
  return 0;
}
  
