#include <iostream>
#include <vector>
#include <map>
#include "opencv2/opencv.hpp"
#define EIGEN_DONT_VECTORIZE
#include "Eigen/SparseLU.h"
#include "ImageFusion.h"

using namespace std;
using namespace cv;
using namespace Eigen;
#define MASK_BG 0
typedef unsigned char uchar;
typedef Eigen::Triplet<double> Trip;

/**
 *	Clamps the input to [0..1].
 *	@return the clamped input
 */
float static inline clamp01(double f)
{
	if (f < 0.0f) return 0.0f;
	else if (f > 1.0f) return 1.0f;
	else return f;
}

bool ImageFuser::verifyMask(const Mat& SM, int tw, int th, int ox, int oy)
{
  int w = SM.cols;
  int h = SM.rows;
	

  // Verify binary mask
  for (int y = 0; y < h; y++)
  {
    for (int x = 0; x < w; x++)
    {
      uchar b = SM.at<uchar>(y,x);
      if (b != MASK_BG && ((x+ox) >= tw || (y+oy) >= th)) {
        cout << "Error: Source mask translates outside target image" << endl;
        return false;
      }
    }
  }

  // Verify no mask on the boundary
  for (int x = 0; x < w; x++) {
    if (SM.at<uchar>(0,x) != MASK_BG || SM.at<uchar>(h-1,x) != MASK_BG) {
      cout << "Warning: Mask must not be set on the image boundary" << endl;
      return false;
    }
  }
  for (int y = 0; y < h; y++) {
    if (SM.at<uchar>(y,0) != MASK_BG || SM.at<uchar>(y,w-1) != MASK_BG) {
      cout << "Warning: Mask must not be set on the image boundary" << endl;
      return false;
    }
  }
  return true;
}

void ImageFuser::imageFusion(const cv::Mat& _bgimg, const cv::Mat& _foreimage,cv::Mat& fusionImage,const cv::Point2i& offset,const cv::Mat& _mask)
{
  bool useMask=false;
  Mat TAlpha,bgimg,foreimage,mask;
  if (!_mask.empty())
  {
    useMask=true;
    mask=_mask;
  }
  cout<<_foreimage.channels()<<' '<<_bgimg.channels()<<endl;
  if (_bgimg.channels()==4)
  {
    bgimg.create(_bgimg.rows,_bgimg.cols,CV_8UC3);
    TAlpha.create(_bgimg.rows,_bgimg.cols,CV_8UC1);
    Mat out[] = { bgimg, TAlpha };
    // rgba[0] -> bgr[2], rgba[1] -> bgr[1],
    // rgba[2] -> bgr[0], rgba[3] -> alpha[0]
    int from_to[] = { 0,0, 1,1, 2,2, 3,3 };
    mixChannels( &_bgimg, 1, out, 2, from_to, 4 );
  }
  else
  {
    bgimg=_bgimg;
  }
  if (_foreimage.channels()==4)
  {
    foreimage.create(_foreimage.rows,_foreimage.cols,CV_8UC3);
    int from_to[] = { 0,0, 1,1, 2,2, 3,3 };
    if (useMask)
    {
      mixChannels( &_foreimage,1,&foreimage,1,from_to,3);
    }
    else
    {
      useMask=true;
      mask.create(_foreimage.rows,_foreimage.cols,CV_8UC1);
      Mat out2[] = { foreimage, mask };
      //cout<<foreimage.channels()<<endl;
      mixChannels( &_foreimage,1,out2,2,from_to,4);
      int w=mask.cols,h=mask.rows;
      for (int x = 0; x < w; x++)
      {
        mask.at<uchar>(0,x) = MASK_BG;
        mask.at<uchar>(h-1,x) = MASK_BG;
      }
      for (int y = 0; y < h; y++)
      {
        mask.at<uchar>(y,0) = MASK_BG;
        mask.at<uchar>(y,w-1) = MASK_BG;
      }
    }
  }
  else
  {
    foreimage=_foreimage;
  }
  
  CV_Assert( !bgimg.empty());
  CV_Assert( !foreimage.empty());
  //CV_Assert( !mask.empty());
  if (useMask)
  {
    CV_Assert(foreimage.cols==mask.cols && foreimage.rows==mask.rows);
    CV_Assert( mask.channels()==1);
    CV_Assert(ImageFuser::verifyMask(mask,bgimg.cols,bgimg.rows, offset.x, offset.y));
  }
  //cout<<foreimage.channels()<<' '<<bgimg.channels()<<endl;
  CV_Assert((foreimage.channels()==bgimg.channels()));
  CV_Assert(bgimg.channels()==3);
  CV_Assert((foreimage.depth()==bgimg.depth()));
  CV_Assert((bgimg.depth()==CV_8U));

  int width=bgimg.cols,height=bgimg.rows;
  Mat TSI(height,width,CV_8UC3),TSM=Mat::zeros(height,width,CV_8UC1);
  int ox=offset.x,oy=offset.y;
  for (uint y = 0; y < height; y++)
    for (uint x = 0; x < width; x++)
    {
      if (x-ox>=0&&(x-ox<foreimage.cols)&&y-oy>=0&&(y-oy<foreimage.rows))
      {
        TSI.at<Vec3b>(y,x)=foreimage.at<Vec3b>(y-oy,x-ox);
        if (useMask)
          TSM.at<uchar>(y,x)=mask.at<uchar>(y-oy,x-ox);
        else
        {
          if (x-ox>0&&y-oy>0&&x-ox<foreimage.cols-1&&y-oy<foreimage.rows-1)
            TSM.at<uchar>(y,x)=255;
        }
      }
    }
  if (useMask)
    imwrite("mask.jpg",TSM);
  Mat TIf,TSIf,lapla,fusionRes;
  bgimg.convertTo(TIf, CV_32FC3, 1.0/255);
  TSI.convertTo(TSIf, CV_32FC3, 1.0/255);
  Laplacian(TSIf,lapla,CV_32F,1);
  ImageFuser::solve(TIf, TSM,lapla, fusionRes);
  if (_bgimg.channels()==4)
  {
    Mat tmpImage;
    fusionRes.convertTo(tmpImage,CV_8UC3,255.0);
    fusionImage.create(height,width,CV_8UC4);
    Mat input[] = { tmpImage, TAlpha };
    int from_to[] = { 0,0, 1,1, 2,2, 3,3 };
    mixChannels( input, 2, &fusionImage, 1, from_to, 4 );
  }
  else
  {
    fusionRes.convertTo(fusionImage,CV_8UC3,255.0);
  }  
}

void ImageFuser::solve(const cv::Mat& I, const cv::Mat& M, const cv::Mat& div, cv::Mat& O)
{
  uint x,y;
  
  uint w = I.cols;
  uint h = I.rows;
  
  // Build mapping from (x,y) to variables
  uint N = 0; // variable indexer
  map<uint,uint> mp;
  for (y = 1; y < h-1; y++)
  {
    for (x = 1; x < w-1; x++)
    {
      uint id = y*w+x;
      if ( (M.at<uchar>(y,x) != MASK_BG) )
      { // Masked pixel
          mp[id] = N;
          N++;
      }
    }
  }
  
  if (N == 0)
  {
    cout << "Solver::solve: No masked pixels found (mask color is non-black)\n";
    return;
  }
  cout << "Solver::solve: Solving " << w << "x" << h << " with " << N << " unknowns" << endl;
  std::vector<Trip> tripletList;
  tripletList.reserve(N*5);
  SparseMatrix<double,ColMajor> At(N,N);
  uint n = 0;
  int index = 0;
  vector<VectorXd> xx(3,VectorXd(N)),b(3,VectorXd(N));
  SparseLU<SparseMatrix<double, ColMajor> >   solver;

  // Populate matrix
  for (y = 1; y < h-1; y++)
  {
    for (x = 1; x < w-1; x++)
    {
      if ( (M.at<uchar>(y,x) != MASK_BG))
      {
        uint id = y*w+x;
        Vec3f bb = div.at<Vec3f>(y,x);
        if ( (M.at<uchar>(y-1,x) != MASK_BG))
        {
          tripletList.push_back(Trip(mp[id-w],n,1.0));
          index++;
        }
        else
        {
          // Known pixel, update right hand side
          bb -= I.at<Vec3f>(y-1,x);
        }
          
        if ( (M.at<uchar>(y,x-1) != MASK_BG))
        {
          tripletList.push_back(Trip(mp[id-1],n,1.0));
          index++;
        }
        else
        {
          bb -= I.at<Vec3f>(y,x-1);
        }
        tripletList.push_back(Trip(mp[id],n,-4.0));
        index++;
          
        if ( (M.at<uchar>(y,x+1) != MASK_BG))
        {
          tripletList.push_back(Trip(mp[id+1],n,1.0));
          index++;
        }
        else
        {
          bb -= I.at<Vec3f>(y,x+1);
        }
          
        if ( (M.at<uchar>(y+1,x) != MASK_BG))
        {
          tripletList.push_back(Trip(mp[id+w],n,1.0));
          index++;
        }
        else
        {
          bb -= I.at<Vec3f>(y+1,x);
        }
          
        uint i = mp[id];
        // Spread the right hand side so we can solve using TAUCS for
        // 3 channels at once.
        for (int kk=0;kk<3;kk++)
        {
          b[kk](i)=bb[kk];
        }
        n++;
      }
    }
  }
  At.setFromTriplets(tripletList.begin(), tripletList.end());
  CV_Assert(n == N);
  At=SparseMatrix< double,ColMajor>(At.transpose());
  cout<<"done populating with "<<At.nonZeros()<<" non-zeros"<<endl;
  solver.analyzePattern(At);
  cout<<"done analyzing"<<endl;
  solver.factorize(At);
  cout<<"done factorize"<<endl;
  for (int kk=0;kk<3;kk++)
  {
    xx[kk] = solver.solve(b[kk]);
    if (solver.info()==Success)
      cout<<"solve succeeded"<<endl;
    else
      cout<<"solve failed"<<endl;
  }
  I.copyTo(O);
  for (y = 1; y < h-1; y++)
  {
    for (x = 1; x < w-1; x++)
    {
      if ( (M.at<uchar>(y,x) != MASK_BG))
      {
        uint id = y*w+x;
        uint ii = mp[id];
        Vec3f &p=O.at<Vec3f>(y,x);
        // cout<<p[0]<<' '<<p[1]<<"  o"<<endl;
        //cout<<xx[0](ii)<<' '<<xx[1](ii)<<"  n"<<endl;
        // Clamp RGB values to [0..1]
        p=Vec3f(clamp01(xx[0](ii)),clamp01(xx[1](ii)),clamp01(xx[2](ii)));
      }
    }
  }
}

