GPP=g++ -g -pg -Wall `pkg-config --libs --cflags opencv` -o
All :	main.o ImageFusion.o
	$(GPP) IF.bin main.o ImageFusion.o
main.o:	main.cpp  ImageFusion.h
	$(GPP) main.o -c main.cpp
ImageFusion.o:	ImageFusion.cpp ImageFusion.h
	$(GPP) ImageFusion.o -c ImageFusion.cpp
