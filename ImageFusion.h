#ifndef IMAGEFUSION_H
#define IMAGEFUSION_H

#include "opencv2/opencv.hpp"

class ImageFuser
{
 public:
  static void imageFusion(const cv::Mat& bgimg, const cv::Mat& foreimage,cv::Mat& fusionImage,const cv::Point2i& offset=cv::Point2i(0,0),const cv::Mat& mask=cv::Mat());
 private:
  static void solve(const cv::Mat& input, const cv::Mat& mask, const cv::Mat& lap, cv::Mat& output);
  static bool verifyMask(const cv::Mat& SM, int tw, int th, int ox, int oy);
};
#endif
